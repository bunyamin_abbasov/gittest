using Microsoft.EntityFrameworkCore;

namespace ATM.Data
{
    class ATMDbContext : DbContext
    {
        public ATMDbContext(options) : base()
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Card> Cards { get; set; }

        public DbSet<Transaction> Transactions { get; set; }     
    }
}