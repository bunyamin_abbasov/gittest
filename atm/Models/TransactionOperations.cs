using System;
using System.Linq;
using ATM.Data;
using System.Collections.Generic;

class TransactionOperation
{
    private readonly ATMDbContext dbContext;

    public TransactionOperation(ATMDbContext dbContext)
    {
        this.dbContext = dbContext;
    }

    public bool Transaction(Card from, Card to, double amount)
    {
        try
        {
            if((decimal)amount > from.Balance)
            {
                throw new Exception("Tranaction amount is more than balance :(");
            }

            var cardFrom = this.dbContext.Cards.FirstOrDefault(c=>c.Id == from.Id);
            cardFrom.Balance -= (decimal)amount;

            var cardTo = this.dbContext.Cards.FirstOrDefault(c=>c.Id == to.Id);
            cardFrom.Balance += (decimal)amount;

            this.dbContext.Transactions.Add(new Transaction(){
                CardFrom = from,
                CardTo = to,
                Amount = (decimal)amount,
                TransactionDate = DateTime.Now
            });
            this.dbContext.SaveChanges();
            return true;
        }
        catch (Exception ex)
        {
            return false;
            throw;
        }

    }

    public List<Transaction> GetTransactionsByDay(Card card, int days = 0)
    {
        if(days == 0)
        {
            var transaction = this.dbContext.Transactions.FirstOrDefault(t=>t.CardFromId == card.Id || t.CardToId==card.Id);
            List<Transaction> lastTransaction =  new List<Transaction>();
            lastTransaction.Add(transaction);
            return lastTransaction;
        }
        var transactions = this.dbContext.Transactions.Where(t=> t.CardFromId == 
                                        card.Id && 
                                        DateTime.Now.AddDays(days) <= t.TransactionDate || 
                                        t.CardToId == card.Id && DateTime.Now.AddDays(days)<=t.TransactionDate);
        return transactions.ToList<Transaction>();
    }
}