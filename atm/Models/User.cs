using System.Collections.Generic;

class User
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Surname { get; set; }
    public Card CreditCard { get; set; }
    public ICollection<Card> Card { get; set; }
}