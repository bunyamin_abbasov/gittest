using System;

class Transaction
{
    public int Id { get; set; }
    public decimal Amount { get; set; }
    public DateTime TransactionDate { get; set; }
    
    public int CardFromId { get; set; }
    public Card CardFrom { get; set; }

    public int CardToId { get; set; }
    public Card CardTo { get; set; }
}