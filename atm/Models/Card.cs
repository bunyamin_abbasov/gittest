using System.Collections.Generic;

class Card
{
    public int Id { get; set; }
    public string PAN { get; set; }
    public string PIN { get; set; }
    public string CVC { get; set; }
    public string ExpireDate { get; set; }
    public decimal Balance { get; set; }

    public ICollection<Transaction> Transaction { get; set; }
    public User User { get; set; }
}